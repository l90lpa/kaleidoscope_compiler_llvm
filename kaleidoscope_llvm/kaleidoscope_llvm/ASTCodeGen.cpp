
#include "ASTCodeGen.hpp"

#include <gsl/gsl>

void CodeGenOperation::EmitTranslationUnit(const TranslationUnitAST* TU) {
	for (const auto decl : TU->body) {
		EmitDecl(decl.get());
	}
}

llvm::Value* CodeGenOperation::EmitExpr(const ExprAST* expr) {
	switch (expr->getKind()) {

	case ASTKind::ast_numberexpr:
		return EmitNumberExpr(static_cast<const NumberExprAST*>(expr));

	case ASTKind::ast_variableexpr:
		return EmitVariableExpr(static_cast<const VariableExprAST*>(expr));

	case ASTKind::ast_binopexpr:
		return EmitBinaryExpr(static_cast<const BinaryExprAST*>(expr));

	case ASTKind::ast_callexpr:
		return EmitCallExpr(static_cast<const CallExprAST*>(expr));

	case ASTKind::ast_ifexpr:
		return EmitIfExpr(static_cast<const IfExprAST*>(expr));

	case ASTKind::ast_forexpr:
		return EmitForExpr(static_cast<const ForExprAST*>(expr));

	default:
		Ensures(false && "Unknown AST expr encountered.");
		return nullptr;
	}
};

llvm::Function* CodeGenOperation::EmitDecl(const DeclAST* decl) {
	switch (decl->getKind()) {
	case ASTKind::ast_prototype:
	{
		auto prototype = static_cast<const PrototypeAST*>(decl);
		return EmitPrototypeDecl(prototype);
	}
	case ASTKind::ast_function:
	{
		auto function = static_cast<const FunctionAST*>(decl);
		return EmitFunctionDecl(function);
	}
	default:
		Ensures(false && "Only prototype and function AST nodes are expected.");
	}
};

llvm::Value* CodeGenOperation::EmitNumberExpr(const NumberExprAST* number) {

	return llvm::ConstantFP::get(context, llvm::APFloat(number->Val));
};

llvm::Value* CodeGenOperation::EmitVariableExpr(const VariableExprAST* variable) {

	auto result = namedValues.find(variable->Name);
	if (result != namedValues.end()) {
		return result->second;
	}
	else {
		return nullptr;
	}
};

llvm::Value* CodeGenOperation::EmitBinaryExpr(const BinaryExprAST* binary) {
	llvm::Value* L = EmitExpr(binary->LHS.get());
	llvm::Value* R = EmitExpr(binary->RHS.get());

	if (!L || !R)
		return nullptr;

	switch (binary->Op) {
	case '+':
		return builder.CreateFAdd(L, R, "addtmp");
	case '-':
		return builder.CreateFSub(L, R, "subtmp");
	case '*':
		return builder.CreateFMul(L, R, "multmp");
	case '/':
		return builder.CreateFDiv(L, R, "divtmp");
	case '<':
		L = builder.CreateFCmpULT(L, R, "ulttmp");
		// Convert bool 0/1 to double 0.0 or 1.0
		return builder.CreateUIToFP(L, llvm::Type::getDoubleTy(context), "booltmp");
	default:
		return LogErrorV("invalid binary operator");
	}
};

llvm::Value* CodeGenOperation::EmitCallExpr(const CallExprAST* call) {
	// Look up the name in the global module table.
	//TODO: reinstate ---> Function* CalleeF = CodeGenOperation::getFunction(call.Callee);
	llvm::Function* CalleeF = llvmModule->getFunction(call->Callee);
	if (!CalleeF)
		return LogErrorV("Unknown function referenced");

	// If argument mismatch error.
	if (CalleeF->arg_size() != call->Args.size())
		return LogErrorV("Incorrect # arguments passed");

	std::vector<llvm::Value*> ArgsV;
	for (unsigned i = 0, e = call->Args.size(); i != e; ++i) {
		ArgsV.push_back(EmitExpr(call->Args[i].get()));
		if (!ArgsV.back())
			return nullptr;
	}

	return builder.CreateCall(CalleeF, ArgsV, "calltmp");
};

llvm::Value* CodeGenOperation::EmitIfExpr(const IfExprAST* If) {
	
	auto CondV = EmitExpr(If->Cond.get());
	if (!CondV) {
		return nullptr;
	}

	// Convert condition expression to a bool by comparing to 0.0.
	CondV = builder.CreateFCmpONE(CondV, llvm::ConstantFP::get(context, llvm::APFloat{ 0.0 }), "ifcond");

	// Get the current function (theParent) that the current basic block (GetInsertBlock) is embedded inside.
	auto function = builder.GetInsertBlock()->getParent();

	// Create blocks for the 'then' and 'else' cases. Inserting the 'then' block at the end of the function.
	llvm::BasicBlock* ThenBB = llvm::BasicBlock::Create(context, "then", function);
	llvm::BasicBlock* ElseBB = llvm::BasicBlock::Create(context, "else");
	llvm::BasicBlock* ifcontBB = llvm::BasicBlock::Create(context, "ifcont");

	builder.CreateCondBr(CondV, ThenBB, ElseBB);

	builder.SetInsertPoint(ThenBB);
	auto ThenV = EmitExpr(If->Then.get());
	if (!ThenV) {
		return nullptr;
	}
	builder.CreateBr(ifcontBB);
	// Codegen of 'Then' can change the current block, update ThenBB for the PHI. I.e. during recursive emission of IR
	// in "EmitExpr" we may have changed the current block we are inserting into (think nested if statements) and so
	// we need to update ThenBB ready for use with the PHI node.
	ThenBB = builder.GetInsertBlock();

	function->getBasicBlockList().push_back(ElseBB);
	
	// Emit else block.
	builder.SetInsertPoint(ElseBB);
	auto ElseV = EmitExpr(If->Else.get());
	if (!ElseV) {
		return nullptr;
	}
	builder.CreateBr(ifcontBB);
	// Codegen of 'Else' can change the current block, update ElseBB for the PHI. Same as ThenBB update above (we may 
	// have added additional blocks during recursive emission of IR during EmitExpr and so ElseBB may not be pointing
	// at the current insertion block).
	ElseBB = builder.GetInsertBlock();

	function->getBasicBlockList().push_back(ifcontBB);
	builder.SetInsertPoint(ifcontBB);

	llvm::PHINode* PN = builder.CreatePHI(llvm::Type::getDoubleTy(context), 2, "iftmp");

	PN->addIncoming(ThenV, ThenBB);
	PN->addIncoming(ElseV, ElseBB);

	return PN;
}

llvm::Value* CodeGenOperation::EmitForExpr(const ForExprAST* For) {
	
	auto StartV = EmitExpr(For->Start.get());
	if (!StartV) {
		return nullptr;
	}

	// Make the new basic block for the loop header, inserting after current
	// block.
	llvm::Function* function = builder.GetInsertBlock()->getParent();
	llvm::BasicBlock* preForBB = builder.GetInsertBlock();
	llvm::BasicBlock* forLoopBB = llvm::BasicBlock::Create(context, "forLoop", function);

	// Insert an explicit fall through from the current block to the LoopBB.
	builder.CreateBr(forLoopBB);

	// Start insertion in LoopBB.
	builder.SetInsertPoint(forLoopBB);

	// Start the PHI node with an entry for Start.
	llvm::PHINode* variable = builder.CreatePHI(llvm::Type::getDoubleTy(context), 2, For->VarName);
	
	variable->addIncoming(StartV, preForBB);

	// Within the loop, the variable is defined equal to the PHI node.  If it
	// shadows an existing variable, we have to restore it, so save it now.
	llvm::Value* OldVal = namedValues[For->VarName];
	namedValues[For->VarName] = variable;

	// Emit the body of the loop.  This, like any other expr, can change the
	// current BB.  Note that we ignore the value computed by the body, but don't
	// allow an error.
	if (!EmitExpr(For->Body.get()))
		return nullptr;

	// Emit the step value.
	llvm::Value* stepVal = nullptr;
	if (For->Step) {
		stepVal = EmitExpr(For->Step.get());
		if (!stepVal)
			return nullptr;
	}
	else {
		// If not specified, use 1.0.
		stepVal = llvm::ConstantFP::get(context, llvm::APFloat(1.0));
	}

	llvm::Value* nextVar = builder.CreateFAdd(variable, stepVal, "nextVar");

	// Compute the end condition.
	llvm::Value* EndCondV = EmitExpr(For->End.get());
	if (!EndCondV)
		return nullptr;

	// Convert condition to a bool by comparing non-equal to 0.0.
	EndCondV = builder.CreateFCmpONE(EndCondV, llvm::ConstantFP::get(context, llvm::APFloat(0.0)), "forLoopCond");

	// Create the "after loop" block and insert it.
	llvm::BasicBlock* forLoopEndBB = builder.GetInsertBlock();
	llvm::BasicBlock* postForLoopBB = llvm::BasicBlock::Create(context, "postForLoop", function);

	// Insert the conditional branch into the end of LoopEndBB.
	builder.CreateCondBr(EndCondV, forLoopBB, postForLoopBB);

	// Any new code will be inserted in AfterBB.
	builder.SetInsertPoint(postForLoopBB);

	// Add a new entry to the PHI node for the backedge.
	variable->addIncoming(nextVar, forLoopEndBB);

	// Restore the unshadowed variable.
	if (OldVal)
		namedValues[For->VarName] = OldVal;
	else
		namedValues.erase(For->VarName);

	// For-Expression always returns 0.0
	return llvm::Constant::getNullValue(llvm::Type::getDoubleTy(context));
}

llvm::Function* CodeGenOperation::EmitPrototypeDecl(const PrototypeAST* prototype) {
	// Make the function type:  double(double,double) etc.
	std::vector<llvm::Type*> Doubles(prototype->Args.size(), llvm::Type::getDoubleTy(context));
	llvm::FunctionType* FT =
		llvm::FunctionType::get(llvm::Type::getDoubleTy(context), Doubles, false);

	llvm::Function* F =
		llvm::Function::Create(FT, llvm::Function::ExternalLinkage, prototype->Name, llvmModule.get());

	// Set names for all arguments.
	unsigned Idx = 0;
	for (auto& Arg : F->args())
		Arg.setName(prototype->Args[Idx++]);

	return F;
};

llvm::Function* CodeGenOperation::EmitFunctionDecl(const FunctionAST* function) {
	// First, check for an existing function from a previous 'extern' declaration.
	llvm::Function* TheFunction = llvmModule->getFunction(function->Proto->getName());

	if (!TheFunction)
		TheFunction = EmitPrototypeDecl(function->Proto.get());

	if (!TheFunction)
		return nullptr;

	// Create a new basic block to start insertion into.
	llvm::BasicBlock* BB = llvm::BasicBlock::Create(context, "entry", TheFunction);
	builder.SetInsertPoint(BB);

	// Record the function arguments in the NamedValues map.
	namedValues.clear();
	for (auto& Arg : TheFunction->args())
		namedValues[Arg.getName()] = &Arg;

	if (llvm::Value * RetVal = EmitExpr(function->Body.get())) {
		// Finish off the function.
		builder.CreateRet(RetVal);

		// Validate the generated code, checking for consistency.
		llvm::verifyFunction(*TheFunction);

		return TheFunction;
	}

	// Error reading body, remove function.
	TheFunction->eraseFromParent();
	return nullptr;
};