
#include "Parser.hpp"

TranslationUnitAST parseAST(Parser& parser) {
	
	// add the translation unit node to the AST

	std::vector<std::shared_ptr<DeclAST>> body;

	auto declResult = parser.ParseTopLevelDecl();
	while (std::holds_alternative<std::shared_ptr<DeclAST>>(declResult)) {

		// add the AST decl to the translation unit

		std::shared_ptr<DeclAST> decl;
		std::swap(decl, std::get<std::shared_ptr<DeclAST>>(declResult));

		body.emplace_back(std::move(decl));

		declResult = parser.ParseTopLevelDecl();
	}

	TranslationUnitAST translationUnit{ "no_name", std::move(body) };

	return translationUnit;
}