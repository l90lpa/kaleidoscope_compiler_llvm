
#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <vector>


enum class ASTKind {
	ast_numberexpr,
	ast_variableexpr,
	ast_binopexpr,
	ast_callexpr,
	ast_ifexpr,
	ast_forexpr,
	ast_prototype,
	ast_function,
	ast_translationUnit
};


//===----------------------------------------------------------------------===//
// Abstract Syntax Tree (aka Parse Tree)
//===----------------------------------------------------------------------===//


/// ExprAST - Base class for all expression nodes.
class ExprAST {
public:
	virtual ~ExprAST() = default;

	virtual ASTKind getKind() const = 0;
};

/// NumberExprAST - Expression class for numeric literals like "1.0".
class NumberExprAST : public ExprAST {
public:
	double Val;

	NumberExprAST(double Val) : Val(Val) {}

	ASTKind getKind() const override { return ASTKind::ast_numberexpr; }
};

/// VariableExprAST - Expression class for referencing a variable, like "a".
class VariableExprAST : public ExprAST {
public:
	std::string Name;

	VariableExprAST(const std::string& Name) : Name(Name) {}

	ASTKind getKind() const override { return ASTKind::ast_variableexpr; }
};

/// BinaryExprAST - Expression class for a binary operator.
class BinaryExprAST : public ExprAST {
public:
	char Op;
	std::shared_ptr<ExprAST> LHS, RHS;

	BinaryExprAST(char Op, std::shared_ptr<ExprAST> LHS,
		std::shared_ptr<ExprAST> RHS)
		: Op(Op), LHS(std::move(LHS)), RHS(std::move(RHS)) {}

	ASTKind getKind() const override { return ASTKind::ast_binopexpr; }
};

/// CallExprAST - Expression class for function calls.
class CallExprAST : public ExprAST {
public:
	std::string Callee;
	std::vector<std::shared_ptr<ExprAST>> Args;

	CallExprAST(const std::string& Callee,
		std::vector<std::shared_ptr<ExprAST>> Args)
		: Callee(Callee), Args(std::move(Args)) {}

	ASTKind getKind() const override { return ASTKind::ast_callexpr; }
};

class IfExprAST : public ExprAST {
public:
	std::shared_ptr<ExprAST> Cond, Then, Else;

	IfExprAST(std::shared_ptr<ExprAST> Cond, std::shared_ptr<ExprAST> Then,
		std::shared_ptr<ExprAST> Else)
		: Cond(std::move(Cond)), Then(std::move(Then)), Else(std::move(Else)) {}

	ASTKind getKind() const override { return ASTKind::ast_ifexpr; }
};

class ForExprAST : public ExprAST {
public:
	std::string VarName;
	std::shared_ptr<ExprAST> Start, End, Step, Body;

	ForExprAST(const std::string& VarName, std::shared_ptr<ExprAST> Start,
		std::shared_ptr<ExprAST> End, std::shared_ptr<ExprAST> Step,
		std::shared_ptr<ExprAST> Body)
		: VarName(VarName), Start(std::move(Start)), End(std::move(End)),
		Step(std::move(Step)), Body(std::move(Body)) {}

	ASTKind getKind() const override { return ASTKind::ast_forexpr; }
};

/// ExprAST - Base class for all expression nodes.
class DeclAST {
public:
	virtual ~DeclAST() = default;

	virtual ASTKind getKind() const = 0;
};

/// PrototypeAST - This class represents the "prototype" for a function,
/// which captures its name, and its argument names (thus implicitly the number
/// of arguments the function takes).
class PrototypeAST : public DeclAST {
public:
	std::string Name;
	std::vector<std::string> Args;

	PrototypeAST(const std::string& Name, std::vector<std::string> Args)
		: Name(Name), Args(std::move(Args)) {}

	const std::string& getName() const { return Name; }

	ASTKind getKind() const override { return ASTKind::ast_prototype; }
};

/// FunctionAST - This class represents a function definition itself.
class FunctionAST : public DeclAST {
public:
	std::shared_ptr<PrototypeAST> Proto;
	std::shared_ptr<ExprAST> Body;

	FunctionAST(std::shared_ptr<PrototypeAST> Proto,
		std::shared_ptr<ExprAST> Body)
		: Proto(std::move(Proto)), Body(std::move(Body)) {}

	ASTKind getKind() const override { return ASTKind::ast_function; }
};


class TranslationUnitAST {
public:
	std::string name;
	std::vector<std::shared_ptr<DeclAST>> body;


	TranslationUnitAST() = default;
	TranslationUnitAST(std::string name, std::vector<std::shared_ptr<DeclAST>>&& body) : name{ name }, body{ std::move(body) } {}

	const std::string_view getName() const {
		return std::string_view{ name };
	}

	ASTKind getKind() const { return ASTKind::ast_translationUnit; }
};