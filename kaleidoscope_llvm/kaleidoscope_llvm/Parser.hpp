
#pragma once

#include <cassert>
#include <map>
#include <memory>
#include <string_view>
#include <variant>

#include <llvm/ADT/STLExtras.h>

#include "AST.hpp"
#include "Lexer.hpp"


//===----------------------------------------------------------------------===//
// Parser
//===----------------------------------------------------------------------===//

/// # ___Grammar___
/// 
/// Notes:
/// - A grammar is defined by providing a 'grammar name' and some combination of literals, terminal grammars and 
///   non-terminal grammars that the grammar will expand into (or conversely will that it will match positively
///   against), effectively its definition. A grammar can have mutliple definitions.
/// - the symbol `::=` separates the grammar name from its definition/s.
/// - the symbol `*` is used that there could be any number of a given statement (0 or more). I.e. expandes to 
///   expression, expression, ...
/// - parenthesis outside of quotes are used to group grammars on the rhs
///
/// Example:
/// ```
/// identifierexpr
///   ::= identifier
///   ::= identifier '(' expression* ')'
/// ```
/// The above grammar is called `identifierexpr` and has 2 definitions: 
///  - The first definition contains a single terminal grammar `identifier` which is the name of a function or variable.
///  - The second definition contains the same terminal grammar followed by the non-terminal grammar `expression` which
///    is enclosed between the literals '(' and ')' and is marked as there could be 0 or more of this statement. 
///
/// ## ___The Kaleidoscope grammar___:
///
/// ### ___Terminal grammars___:
/// - number, a double floating-point number
/// - binop, one of the operators {'+','-','*','/'}
/// - identifier, the name of a function or variable
///
/// ### ___Non-terminal grammars___:
///
/// identifierexpr
///   ::= identifier
///   ::= identifier '(' expression* ')'
/// numberexpr ::= number
/// parenexpr ::= '(' expression ')'
/// ifexpr ::= 'if' expression 'then' expression 'else' expression
/// forexpr ::= 'for' identifier '=' expression ',' expression (',' expression)? 'in' expression
/// primary
///   ::= identifierexpr
///   ::= numberexpr
///   ::= parenexpr
///   ::= ifexpr
///   ::= forexpr
/// binoprhs
///   ::= ('+' primary)*
/// expression
///   ::= primary binoprhs
/// prototype
///   ::= identifier '(' identifier* ')'
/// definition ::= 'def' prototype expression
/// toplevelexpr ::= expression
/// external ::= 'extern' prototype
/// top 
///   ::= definition
///   ::= external
///   ::= expression
///   ::= ';'

struct EndOfFile {};

class Parser {
public:
	Parser(Lexer& lexer, const std::unordered_map<char, int>& binOpPrecedence) : lexer_{ lexer }, binOpPrecedence_{ binOpPrecedence } {}

	Lexer& lexer_;
	const std::unordered_map<char, int>& binOpPrecedence_;

	Token currToken;

	// Install standard binary operators. 
	// 0 is lowest precedence +inf is highest precedence.

	Token getNextToken() { return currToken = lexer_.getToken(); }
	Token getCurrToken() { return currToken;  }

private:
	/// getTokenPrecedence - Get the precedence of the pending binary operator token.
	int getTokenPrecedence() const {

		if (currToken.string.has_value()) {
			std::string_view tokenStr = currToken.string.value();

			char binop = tokenStr.front();

			if (tokenStr.size() != 1 || !isascii(binop))
				return -1;

			// Make sure it's a declared binop.
			if (auto result = binOpPrecedence_.find(tokenStr.front()); result != binOpPrecedence_.end()) {
				return result->second;
			}
			else {
				return -1;
			}
		}
		else {
			return -1;
		}
	}

	/// LogError* - These are little helper functions for error handling.
	std::shared_ptr<ExprAST> LogError(const char* Str) {
		fprintf(stderr, "Error: %s\n", Str);
		return nullptr;
	}
	std::shared_ptr<PrototypeAST> LogErrorP(const char* Str) {
		LogError(Str);
		return nullptr;
	}

	/// numberexpr ::= number
	std::shared_ptr<ExprAST> ParseNumberExpr() {
		auto Result = std::make_shared<NumberExprAST>(currToken.numeric.value());
		getNextToken(); // consume the number
		return std::move(Result);
	}

	/// parenexpr ::= '(' expression ')'
	std::shared_ptr<ExprAST> ParseParenExpr() {
		getNextToken(); // eat (.
		auto V = ParseExpression();
		if (!V)
			return nullptr;

		if (currToken.kind != TokenKind::tok_r_paren) {
			return LogError("expected ')'");
		}

		getNextToken(); // eat ).
		return V;
	}

	/// identifierexpr
	///   ::= identifier
	///   ::= identifier '(' expression* ')'
	std::shared_ptr<ExprAST> ParseIdentifierExpr() {
		std::string IdName = currToken.string.value();

		getNextToken(); // eat identifier.

		// Simple variable ref.
		if (currToken.kind != TokenKind::tok_l_paren) {
			return std::make_shared<VariableExprAST>(IdName);
		}

		// Call.
		getNextToken(); // eat (
		std::vector<std::shared_ptr<ExprAST>> Args;
		if (currToken.kind != TokenKind::tok_r_paren) {
			while (true) {
				if (auto Arg = ParseExpression())
					Args.push_back(std::move(Arg));
				else
					return nullptr;

				if (currToken.kind == TokenKind::tok_r_paren)
					break;

				if (currToken.kind != TokenKind::tok_comma)
					return LogError("Expected ')' or ',' in argument list");
				getNextToken();
			}
		}

		// Eat the ')'.
		getNextToken();

		return std::make_shared<CallExprAST>(IdName, std::move(Args));
	}

	/// ifexpr ::= 'if' expression 'then' expression 'else' expression
	std::shared_ptr<ExprAST> ParseIfExpr() {
		getNextToken(); // Eat the 'if' token.

		// Handle the condition
		auto Cond = ParseExpression();
		if (!Cond) { 
			return nullptr;
		}
		
		if (currToken.kind != TokenKind::tok_then) {
			return LogError("expected \'then\' token");
		}
		getNextToken(); // Eat the 'then' token.

		// Handle the then
		auto Then = ParseExpression();
		if (!Then) {
			return nullptr;
		}

		if (currToken.kind != TokenKind::tok_else) {
			return LogError("expected \'else\' token");
		}
		getNextToken(); // Eat the 'else' token.

		// Handle the else
		auto Else = ParseExpression();
		if (!Else) {
			return nullptr;
		}

		return std::make_shared<IfExprAST>(std::move(Cond), std::move(Then), std::move(Else));
	}

	/// forexpr ::= 'for' identifier '=' expression ',' expression (',' expression)? 'in' expression
	std::shared_ptr<ExprAST> ParseForExpr() {
		getNextToken(); // Eat the 'for' token.

		// Handle the identifier
		if (currToken.kind != TokenKind::tok_identifier) {
			return LogError("expected an identifier after for");
		}

		assert(currToken.string.has_value() && "the token is an identifier and so then it should have a string");
		std::string IdName = currToken.string.value();

		getNextToken(); // eat the identifer

		if (currToken.kind != TokenKind::tok_equal) {
			return LogError("expected an \'=\' after for identifer");
		}
		getNextToken(); // eat the `=`

		auto Start = ParseExpression();
		if (!Start) {
			return nullptr;
		}
		
		if (currToken.kind != TokenKind::tok_comma) {
			return LogError("expected \',\' after for loop initializer");
		}
		getNextToken(); // eat the `,`

		auto End = ParseExpression();
		if (!End) {
			return nullptr;
		}

		std::shared_ptr<ExprAST> Step = nullptr;
		if (currToken.kind == TokenKind::tok_comma) {
			getNextToken(); // eat the `,`
			Step = ParseExpression();
			if (!Step) {
				return nullptr;
			}
		}

		if (currToken.kind != TokenKind::tok_in) {
			return LogError("expected \'in\' prior to the for-loop body");
		}
		getNextToken(); // eat the `in`

		auto Body = ParseExpression();
		if (!Body) {
			return nullptr;
		}

		return std::make_shared<ForExprAST>(IdName, std::move(Start), std::move(End), std::move(Step), 
											std::move(Body));
	}

	/// primary
	///   ::= identifierexpr
	///   ::= numberexpr
	///   ::= parenexpr
	///   ::= ifexpr
	std::shared_ptr<ExprAST> ParsePrimary() {
		switch (currToken.kind) {
		default:
			return LogError("unknown token when expecting an expression");
		case TokenKind::tok_identifier:
			return ParseIdentifierExpr();
		case TokenKind::tok_number:
			return ParseNumberExpr();
		case TokenKind::tok_l_paren:
			return ParseParenExpr();
		case TokenKind::tok_if:
			return ParseIfExpr();
		case TokenKind::tok_for:
			return ParseForExpr();
		}
	}

	/// binoprhs
	///   ::= ('+' primary)*
	std::shared_ptr<ExprAST> ParseBinOpRHS(int ExprPrec,
		std::shared_ptr<ExprAST> LHS) {
		// If this is a binop, find its precedence.
		while (true) {
			int TokPrec = getTokenPrecedence();

			// If this is a binop that binds at least as tightly as the current binop,
			// consume it, otherwise we are done.
			if (TokPrec < ExprPrec)
				return LHS;

			// Okay, we know this is a binop.
			char binOpChar = currToken.string.value().front();
			getNextToken(); // eat binop

			// Parse the primary expression after the binary operator.
			auto RHS = ParsePrimary();
			if (!RHS)
				return nullptr;

			// If BinOp binds less tightly with RHS than the operator after RHS, let
			// the pending operator take RHS as its LHS.
			int NextPrec = getTokenPrecedence();
			if (TokPrec > NextPrec) {
				RHS = ParseBinOpRHS(TokPrec + 1, std::move(RHS));
				if (!RHS)
					return nullptr;
			}

			// Merge LHS/RHS.
			LHS = std::make_shared<BinaryExprAST>(binOpChar, std::move(LHS),
				std::move(RHS));
		}
	}

	/// expression
	///   ::= primary binoprhs
	std::shared_ptr<ExprAST> ParseExpression() {
		auto LHS = ParsePrimary();
		if (!LHS)
			return nullptr;

		return ParseBinOpRHS(0, std::move(LHS));
	}

	/// prototype
	///   ::= identifier '(' identifier* ')'
	std::shared_ptr<PrototypeAST> ParsePrototype() {
		if (currToken.kind != TokenKind::tok_identifier)
			return LogErrorP("Expected function name in prototype");

		std::string FnName = currToken.string.value();
		getNextToken();

		if (currToken.kind != TokenKind::tok_l_paren)
			return LogErrorP("Expected '(' in prototype");

		std::vector<std::string> ArgNames;
		while (getNextToken().kind == TokenKind::tok_identifier)
			ArgNames.push_back(currToken.string.value());
		if (currToken.kind != TokenKind::tok_r_paren)
			return LogErrorP("Expected ')' in prototype");

		// success.
		getNextToken(); // eat ')'.

		return std::make_shared<PrototypeAST>(FnName, std::move(ArgNames));
	}

	/// definition ::= 'def' prototype expression
	std::shared_ptr<FunctionAST> ParseDefinition() {
		getNextToken(); // eat def.
		auto Proto = ParsePrototype();
		if (!Proto)
			return nullptr;

		if (auto E = ParseExpression())
			return std::make_shared<FunctionAST>(std::move(Proto), std::move(E));
		return nullptr;
	}

	/// toplevelexpr ::= expression
	std::shared_ptr<FunctionAST> ParseTopLevelExpr() {
		if (auto E = ParseExpression()) {
			// Make an anonymous proto.
			auto Proto = std::make_shared<PrototypeAST>("__anon_expr",
				std::vector<std::string>());
			return std::make_shared<FunctionAST>(std::move(Proto), std::move(E));
		}
		return nullptr;
	}

	/// external ::= 'extern' prototype
	std::shared_ptr<PrototypeAST> ParseExtern() {
		getNextToken(); // eat extern.
		return ParsePrototype();
	}


public:
	//===----------------------------------------------------------------------===//
	// Top-Level parsing
	//===----------------------------------------------------------------------===//

	std::shared_ptr<FunctionAST> HandleDefinition() {

		auto astNode = ParseDefinition();

		if (astNode) {
			fprintf(stderr, "Parsed a function definition.\n");
		}
		else {
			// Skip token for error recovery.
			getNextToken();
		}

		return astNode;
	}

	std::shared_ptr<PrototypeAST> HandleExtern() {
		
		auto astNode = ParseExtern();
		
		if (ParseExtern()) {
			fprintf(stderr, "Parsed an extern\n");
		}
		else {
			// Skip token for error recovery.
			getNextToken();
		}

		return astNode;
	}

	std::shared_ptr<FunctionAST> HandleTopLevelExpression() {
		// Evaluate a top-level expression into an anonymous function.

		auto astNode = ParseTopLevelExpr();

		if (astNode) {
			fprintf(stderr, "Parsed a top-level expr\n");
		}
		else {
			// Skip token for error recovery.
			getNextToken();
		}

		return astNode;
	}

	std::variant<std::shared_ptr<DeclAST>, EndOfFile> ParseTopLevelDecl() {

		getNextToken();

		// Ignore top-level semicolons
		while (getCurrToken().kind == TokenKind::tok_semicolon) {
			getNextToken();
		}

		switch (getCurrToken().kind) {
		case TokenKind::tok_eof:
			return EndOfFile{};
		case TokenKind::tok_def:
			return HandleDefinition();
		case TokenKind::tok_extern:
			return HandleExtern();
		default:
			return HandleTopLevelExpression();
		}
	}
};


/// top ::= definition | external | expression | ';'
TranslationUnitAST parseAST(Parser& parser);