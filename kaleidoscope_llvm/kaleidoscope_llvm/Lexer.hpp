
#pragma once

#include "Token.hpp"

class Lexer {
public:
	std::iostream& stream_;

	Lexer(std::iostream& stream) : stream_{ stream } {}

	/// getToken - Return the next token from the stream.
	Token getToken();

	Lexer& rdStream(std::iostream& stream);

private:
	static const int initialChar = ' ';
	int prevChar = initialChar;

	Lexer& reset();

	void discardWhiteSpaceBlock();

	void discardCommentLine();

	std::string getIdentifierString();

	std::optional<TokenKind> matchesKeyword(const std::string& identifier) const;

	Token tokenizeIdentifier();

	std::string getNumberString();

	Token tokenizeNumber();
};