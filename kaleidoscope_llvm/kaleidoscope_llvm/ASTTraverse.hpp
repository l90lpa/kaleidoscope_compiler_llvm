#pragma once

#include "AST.hpp"

// ___Depth First Traversing AST Visitor___:
// - When inheriting from this the intension is simply for the private methods `do_preVisit` and `do_postVisit` to be 
// overridden for your needs but that the `traverse` methods which are public and non-virtual shouldn't be circumvented
// or 'overloaded'.
// - The return value from `do_preVisit` method can be used to stop further depth traversal from the given node. Hence 
// it could be used in a derived class to cause early termination of traversal.
// - Finally this design was choosen to decouple the visitor from the AST, i.e. no requirement for the AST to know about
// the visitor.

class DepthFirstASTTraverse {

	virtual bool do_preVisit(const NumberExprAST* number);
	virtual bool do_preVisit(const VariableExprAST* variable);
	virtual bool do_preVisit(const BinaryExprAST* binary);
	virtual bool do_preVisit(const CallExprAST* call);
	virtual bool do_preVisit(const IfExprAST* If);
	virtual bool do_preVisit(const ForExprAST* If);
	virtual bool do_preVisit(const PrototypeAST* prototype);
	virtual bool do_preVisit(const FunctionAST* function);
	virtual bool do_preVisit(const ExprAST* function);
	virtual bool do_preVisit(const DeclAST* function);

	virtual void do_postVisit(const NumberExprAST* number);
	virtual void do_postVisit(const VariableExprAST* variable);
	virtual void do_postVisit(const BinaryExprAST* binary);
	virtual void do_postVisit(const CallExprAST* call);
	virtual void do_postVisit(const IfExprAST* If);
	virtual void do_postVisit(const ForExprAST* If);
	virtual void do_postVisit(const PrototypeAST* prototype);
	virtual void do_postVisit(const FunctionAST* function);
	virtual void do_postVisit(const ExprAST* function);
	virtual void do_postVisit(const DeclAST* function);

public:

	void traverse(const NumberExprAST* number);
	void traverse(const VariableExprAST* variable);
	void traverse(const BinaryExprAST* binary);
	void traverse(const CallExprAST* call);
	void traverse(const IfExprAST* If);
	void traverse(const ForExprAST* If);
	void traverse(const PrototypeAST* prototype);
	void traverse(const FunctionAST* function);
	void traverse(const ExprAST* expr);
	void traverse(const DeclAST* decl);
};