#pragma once

#include <optional>

#include "TokenKind.hpp"


struct Token {
	TokenKind kind;
	std::optional<std::string> string;
	std::optional<double> numeric;
};