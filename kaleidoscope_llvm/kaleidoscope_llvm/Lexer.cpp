
#include "Lexer.hpp"

#include <cctype>
#include <iostream>

Token Lexer::getToken() {

	discardWhiteSpaceBlock();

	Token currToken;

	if (std::isalpha(prevChar)) { // identifier: [a-zA-Z][a-zA-Z0-9]*
		return tokenizeIdentifier();
	}

	if (std::isdigit(prevChar) || prevChar == '.') { // number: [0-9.]+
		return tokenizeNumber();
	}

	if (prevChar == '#') { // comment: ^#.$
		discardCommentLine();

		if (prevChar != EOF)
			// recursive: this is only recursive for multiple comment lines separated by no code
			return getToken();
	}

	// Check for end of file.  Don't eat the EOF.
	if (prevChar == EOF) {
		currToken.kind = TokenKind::tok_eof;
		return currToken;
	}

	// Otherwise, just return the character as its ascii value.
	int currChar = prevChar;
	prevChar = stream_.get();

	if (auto result = punctuationToTokenKind.find(currChar); result != punctuationToTokenKind.end()) {
		currToken.kind = result->second;
		currToken.string = std::string{ (char)currChar };
	}
	else {
		currToken.kind = TokenKind::tok_unknown;
		currToken.string = std::string{ (char)currChar };
	}

	return currToken;
}

Lexer& Lexer::rdStream(std::iostream& stream) {
	this->stream_.rdbuf( stream.rdbuf() );
	return reset();
}

Lexer& Lexer::reset() {
	prevChar = initialChar;
	return *this;
}

void Lexer::discardWhiteSpaceBlock() {
	while (std::isspace(prevChar))
		prevChar = stream_.get();
}

void Lexer::discardCommentLine() {
	do
		prevChar = stream_.get();
	while (prevChar != EOF && prevChar != '\n' && prevChar != '\r');
}

std::string Lexer::getIdentifierString() {
	std::string IdentifierStr = std::string{ (char)prevChar };
	while (std::isalnum((prevChar = stream_.get()))) {
		IdentifierStr += prevChar;
	}
	return IdentifierStr;
}

std::optional<TokenKind> Lexer::matchesKeyword(const std::string& identifier) const {
	auto result = keywordToTokenKind.find(identifier);
	if (result != keywordToTokenKind.end()) {
		return result->second;
	}
	else {
		return {};
	}
}

Token Lexer::tokenizeIdentifier() {
	auto IdentifierStr = getIdentifierString();

	auto matchResult = matchesKeyword(IdentifierStr);

	Token token;

	token.kind = (matchResult.has_value()) ? matchResult.value() : TokenKind::tok_identifier;
	token.string = std::move(IdentifierStr);

	return token;
}

std::string Lexer::getNumberString() {
	std::string numStr;
	do {
		numStr += prevChar;
		prevChar = stream_.get();
	} while (std::isdigit(prevChar) || prevChar == '.');
	return numStr;
}

Token Lexer::tokenizeNumber() {
	std::string NumStr = getNumberString();

	double NumVal = std::strtod(NumStr.c_str(), nullptr);

	Token token;
	token.kind = TokenKind::tok_number;
	token.numeric = NumVal;

	return token;
}