#pragma once

#include <string>
#include <unordered_map>

enum class TokenKind {
	tok_unknown = 0,

	tok_eof,

	// commands
	tok_def, // `def`
	tok_extern, // `extern`

	// control flow
	tok_if, // `if`
	tok_then, // `then`
	tok_else, // `else`
	tok_for, // `for`
	tok_in, // `in`

	// primary
	tok_identifier, // string starting with a letter
	tok_number, // double floating-point number

	// punctuation
	tok_l_paren, // `(`
	tok_r_paren, // `)`
	tok_comma, // `,` 
	tok_semicolon, // `;`
	tok_comment, // `#`

	tok_lessthan, // `<`
	tok_addition, // `+`
	tok_subtraction, // `-`
	tok_multiplication, // `*`
	tok_division, // `/`

	tok_equal // `=`
};

const std::unordered_map<std::string, TokenKind> keywordToTokenKind{
	{"def", TokenKind::tok_def},
	{"extern", TokenKind::tok_extern},
	{"if", TokenKind::tok_if},
	{"then", TokenKind::tok_then},
	{"else", TokenKind::tok_else},
	{"for", TokenKind::tok_for},
	{"in", TokenKind::tok_in} };

const std::unordered_map<char, TokenKind> punctuationToTokenKind{
	{'(', TokenKind::tok_l_paren},
	{')', TokenKind::tok_r_paren},
	{',', TokenKind::tok_comma},
	{';', TokenKind::tok_semicolon },
	{'<', TokenKind::tok_lessthan },
	{'+', TokenKind::tok_addition},
	{'-', TokenKind::tok_subtraction},
	{'*', TokenKind::tok_multiplication},
	{'/', TokenKind::tok_division},
	{'=', TokenKind::tok_equal} };

