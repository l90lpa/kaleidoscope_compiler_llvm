
#include "ASTVisualizer.hpp"

void PrintAST::addHomogenousCharBlock(std::ostream& ostream, const size_t count, char character) {
	for (size_t i = 0; i < count; ++i) {
		ostream << character;
	}
}

void PrintAST::addOffset(std::ostream& ostream, const size_t count) {
	addHomogenousCharBlock(ostream, count, ' ');
}

bool PrintAST::printASTNode(const std::string& name) {
	treeDepth += 1;
	addOffset(ostream, treeDepth);
	ostream << name << "\n";
	return true;
}

void PrintAST::printTranslationUnit(const TranslationUnitAST* TU) {
	treeDepth = 0;

	if (TU == nullptr) {
		return;
	}

	ostream << "translationUnit\n";
	for (const auto decl : TU->body) {
		traverse(decl.get());
	}
}

bool PrintAST::do_preVisit(const NumberExprAST* number) {
	return printASTNode("number");
};
bool PrintAST::do_preVisit(const VariableExprAST* variable) {
	return printASTNode("variable");
};
bool PrintAST::do_preVisit(const BinaryExprAST* binary) {
	return printASTNode("binary");
};
bool PrintAST::do_preVisit(const CallExprAST* call) {
	return printASTNode("call");
};
bool PrintAST::do_preVisit(const IfExprAST* If) {
	return printASTNode("if");
};
bool PrintAST::do_preVisit(const ForExprAST* For) {
	return printASTNode("for");
};
bool PrintAST::do_preVisit(const PrototypeAST* prototype) {
	return printASTNode("prototype");
};
bool PrintAST::do_preVisit(const FunctionAST* function) {
	return printASTNode("function");
};

void PrintAST::do_postVisit(const NumberExprAST* number) { treeDepth -= 1; };
void PrintAST::do_postVisit(const VariableExprAST* variable) { treeDepth -= 1; };
void PrintAST::do_postVisit(const BinaryExprAST* binary) { treeDepth -= 1; };
void PrintAST::do_postVisit(const CallExprAST* call) { treeDepth -= 1; };
void PrintAST::do_postVisit(const IfExprAST* If) { treeDepth -= 1; };
void PrintAST::do_postVisit(const ForExprAST* If) { treeDepth -= 1; };
void PrintAST::do_postVisit(const PrototypeAST* prototype) { treeDepth -= 1; };
void PrintAST::do_postVisit(const FunctionAST* function) { treeDepth -= 1; };