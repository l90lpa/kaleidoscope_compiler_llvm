
#pragma once

#include "AST.hpp"
#include "ASTTraverse.hpp"

#include <stack>
#include <unordered_map>

#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/STLExtras.h>

#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>

#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>

#include <llvm/IR/Type.h>
#include <llvm/IR/Verifier.h>


class CodeGenOperation {
	llvm::LLVMContext& context;
	llvm::IRBuilder<>& builder;
	std::shared_ptr<llvm::Module> llvmModule;
	std::unordered_map<std::string, llvm::Value*>& namedValues;

public:
	CodeGenOperation(
		llvm::LLVMContext& context, llvm::IRBuilder<>& builder, std::shared_ptr<llvm::Module> llvmModule,
		std::unordered_map<std::string, llvm::Value*>& namedValues) : 
		context{ context }, builder{ builder }, llvmModule{ llvmModule }, namedValues{ namedValues } { }

	void EmitTranslationUnit(const TranslationUnitAST* TU);

	llvm::Value* EmitExpr(const ExprAST* expr);

	llvm::Function* EmitDecl(const DeclAST* decl);

	llvm::Value* EmitNumberExpr(const NumberExprAST* number);

	llvm::Value* EmitVariableExpr(const VariableExprAST* variable);

	llvm::Value* EmitBinaryExpr(const BinaryExprAST* binary);

	llvm::Value* EmitCallExpr(const CallExprAST* call);

	llvm::Value* EmitIfExpr(const IfExprAST* If);

	llvm::Value* EmitForExpr(const ForExprAST* For);

	llvm::Function* EmitPrototypeDecl(const PrototypeAST* prototype);

	llvm::Function* EmitFunctionDecl(const FunctionAST* function);

private:

	//llvm::Function* getFunction(std::string Name) {
	//	// First, see if the function has already been added to the current module.
	//	if (auto * F = llvmModule->getFunction(Name))
	//		return F;

	//	// If not, check whether we can codegen the declaration from some existing
	//	// prototype.
	//	auto FI = FunctionProtos.find(Name);
	//	if (FI != FunctionProtos.end())
	//		return FI->second->codegen();

	//	// If no existing prototype exists, return null.
	//	return nullptr;
	//}

	llvm::Value* LogErrorV(const char* Str) {
		fprintf(stderr, "Error: %s\n", Str);
		return nullptr;
	}
};