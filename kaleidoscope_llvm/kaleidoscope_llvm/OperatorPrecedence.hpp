#pragma once 

#include <unordered_map>

// The highest precendence is 1 and the lowes is inf.
// The default precendence is taken from C++
const std::unordered_map<char, int> DefaultBinOpPrecedence{ 
	{'<', 9},
	{'+', 6},
	{'-', 6},
	{'*', 5},
	{'/', 5} };