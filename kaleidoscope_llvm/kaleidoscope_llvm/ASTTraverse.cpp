
#include "ASTTraverse.hpp"

#include <gsl/gsl_assert>

bool DepthFirstASTTraverse::do_preVisit(const NumberExprAST* number) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const VariableExprAST* variable) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const BinaryExprAST* binary) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const CallExprAST* call) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const IfExprAST* If) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const ForExprAST* If) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const PrototypeAST* prototype) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const FunctionAST* function) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const ExprAST* function) { return true; };
bool DepthFirstASTTraverse::do_preVisit(const DeclAST* function) { return true; };

void DepthFirstASTTraverse::do_postVisit(const NumberExprAST* number) {};
void DepthFirstASTTraverse::do_postVisit(const VariableExprAST* variable) {};
void DepthFirstASTTraverse::do_postVisit(const BinaryExprAST* binary) {};
void DepthFirstASTTraverse::do_postVisit(const CallExprAST* call) {};
void DepthFirstASTTraverse::do_postVisit(const IfExprAST* If) {};
void DepthFirstASTTraverse::do_postVisit(const ForExprAST* If) {};
void DepthFirstASTTraverse::do_postVisit(const PrototypeAST* prototype) {};
void DepthFirstASTTraverse::do_postVisit(const FunctionAST* function) {};
void DepthFirstASTTraverse::do_postVisit(const ExprAST* function) {};
void DepthFirstASTTraverse::do_postVisit(const DeclAST* function) {};

void DepthFirstASTTraverse::traverse(const NumberExprAST* number) { 
	/* has no childern */
	do_preVisit(number);
	do_postVisit(number);
};
void DepthFirstASTTraverse::traverse(const VariableExprAST* variable) { 
	/* has no childern */
	do_preVisit(variable);
	do_postVisit(variable);
};
void DepthFirstASTTraverse::traverse(const BinaryExprAST* binary) {
	if (do_preVisit(binary)) {
		traverse(binary->LHS.get());
		traverse(binary->RHS.get());
	}
	do_postVisit(binary);
};
void DepthFirstASTTraverse::traverse(const CallExprAST* call) {
	if (do_preVisit(call)) {
		for (const auto arg : call->Args) {
			traverse(arg.get());
		}
	}
	do_postVisit(call);
};
void DepthFirstASTTraverse::traverse(const IfExprAST* If) {
	if (do_preVisit(If)) {
		traverse(If->Cond.get());
		traverse(If->Then.get());
		traverse(If->Else.get());
	}
	do_postVisit(If);
};
void DepthFirstASTTraverse::traverse(const ForExprAST* For) {
	if (do_preVisit(For)) {
		traverse(For->Start.get());
		traverse(For->End.get());
		if (For->Step) {
			traverse(For->Step.get());
		}
		traverse(For->Body.get());
	}
	do_postVisit(For);
};
void DepthFirstASTTraverse::traverse(const PrototypeAST* prototype) { 
	/* has no childern */	
	do_preVisit(prototype);
	do_postVisit(prototype);
};
void DepthFirstASTTraverse::traverse(const FunctionAST* function) {
	if (do_preVisit(function)) {
		traverse(function->Proto.get());
		traverse(function->Body.get());
	}
	do_postVisit(function);
};


void DepthFirstASTTraverse::traverse(const ExprAST* expr) {
	if (do_preVisit(expr)) {

		switch (expr->getKind()) {
		case ASTKind::ast_numberexpr:
		{
			auto numberExpr = static_cast<const NumberExprAST*>(expr);
			traverse(numberExpr);
			break;
		}
		case ASTKind::ast_variableexpr:
		{
			auto variableExpr = static_cast<const VariableExprAST*>(expr);
			traverse(variableExpr);
			break;
		}
		case ASTKind::ast_binopexpr:
		{
			auto binayExpr = static_cast<const BinaryExprAST*>(expr);
			traverse(binayExpr);
			break;
		}
		case ASTKind::ast_callexpr:
		{
			auto callExpr = static_cast<const CallExprAST*>(expr);
			traverse(callExpr);
			break;
		}
		case ASTKind::ast_ifexpr:
		{
			auto ifExpr = static_cast<const IfExprAST*>(expr);
			traverse(ifExpr);
			break;
		}
		case ASTKind::ast_forexpr:
		{
			auto forExpr = static_cast<const ForExprAST*>(expr);
			traverse(forExpr);
			break;
		}
		default:
			Ensures(false && "Only prototype and function AST nodes are expected.");
		}
	}
	do_postVisit(expr);
};

void DepthFirstASTTraverse::traverse(const DeclAST* decl) {
	if (do_preVisit(decl)) {

		switch (decl->getKind()) {
		case ASTKind::ast_prototype:
		{
			auto prototype = static_cast<const PrototypeAST*>(decl);
			traverse(prototype);
			break;
		}
		case ASTKind::ast_function:
		{
			auto function = static_cast<const FunctionAST*>(decl);
			traverse(function);
			break;
		}
		default:
			Ensures(false && "Only prototype and function AST nodes are expected.");
		}
	}
	do_postVisit(decl);
};