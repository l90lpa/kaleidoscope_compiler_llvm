﻿# CMakeList.txt : CMake project for kaleidoscope_llvm, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

set(SRC_FILES 
	"AST.hpp"
	"ASTCodeGen.hpp"
	"ASTCodeGen.cpp"
	"ASTTraverse.hpp"
	"ASTTraverse.cpp"
	"ASTVisualizer.hpp"
	"ASTVisualizer.cpp"
	"Lexer.hpp"
	"Lexer.cpp"
	"OperatorPrecedence.hpp"
	"Parser.hpp"
	"Parser.cpp"
	"Token.hpp"
	"TokenKind.hpp"
	"main.cpp"
)

# Add source to this project's executable.
add_executable (kaleidoscope_llvm ${SRC_FILES})

find_path(MS_GSL_INCLUDE_DIR "gsl/gsl")
message("MS_GSL_INCLUDE_DIR=${MS_GSL_INCLUDE_DIR}")

target_include_directories(kaleidoscope_llvm PUBLIC ${LLVM_INCLUDE_DIR} ${MS_GSL_INCLUDE_DIR})

target_link_libraries(kaleidoscope_llvm PUBLIC LLVMCore)
