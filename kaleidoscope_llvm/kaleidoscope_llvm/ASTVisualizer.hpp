#pragma once

#include "AST.hpp"
#include "ASTTraverse.hpp"

#include <iostream>
#include <string>

class PrintAST : public DepthFirstASTTraverse {

	std::ostream& ostream;
	size_t treeDepth = 0;

	void addHomogenousCharBlock(std::ostream& ostream, const size_t count, char character);
	void addOffset(std::ostream& ostream, const size_t count);
	bool printASTNode(const std::string& name);

	bool do_preVisit(const NumberExprAST* number) override;
	bool do_preVisit(const VariableExprAST* variable) override;
	bool do_preVisit(const BinaryExprAST* binary) override;
	bool do_preVisit(const CallExprAST* call) override;
	bool do_preVisit(const IfExprAST* If) override;
	bool do_preVisit(const ForExprAST* If) override;
	bool do_preVisit(const PrototypeAST* prototype) override;
	bool do_preVisit(const FunctionAST* function) override;

	void do_postVisit(const NumberExprAST* number) override;
	void do_postVisit(const VariableExprAST* variable) override;
	void do_postVisit(const BinaryExprAST* binary) override;
	void do_postVisit(const CallExprAST* call) override;
	void do_postVisit(const IfExprAST* If) override;
	void do_postVisit(const ForExprAST* If) override;
	void do_postVisit(const PrototypeAST* prototype) override;
	void do_postVisit(const FunctionAST* function) override;

public:
	PrintAST(std::ostream& ostream) : ostream{ ostream } {}

	void printTranslationUnit(const TranslationUnitAST* TU);

};