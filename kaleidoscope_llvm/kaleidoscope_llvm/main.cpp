﻿

#include "Parser.hpp"

#include "AST.hpp"
#include "ASTTraverse.hpp"
#include "ASTVisualizer.hpp"
#include "ASTCodeGen.hpp"

#include "OperatorPrecedence.hpp"

#include <iostream>
#include <sstream>


//===----------------------------------------------------------------------===//
// Main driver code.
//===----------------------------------------------------------------------===//

int main() {
	
	std::string code{ "def foo(x y) x+foo(y, 4.0); foo(1.0, 1.0);if 1 then 10 else 11; for i = 10, i in i - 1;" };

	std::stringstream codestream;
	codestream.str(code);

	Lexer lexer{ codestream };

	TranslationUnitAST unit;

	Parser parser{ lexer, DefaultBinOpPrecedence };

	auto astRoot = parseAST(parser);

	PrintAST printer{ std::cout };

	printer.printTranslationUnit(&astRoot);

	llvm::LLVMContext context;
	llvm::IRBuilder<> builder{ context };
	std::shared_ptr<llvm::Module> llvmModule = std::make_shared<llvm::Module>("singleModule", context);
	std::unordered_map<std::string, llvm::Value*> namedValues;

	CodeGenOperation codegen(context, builder, llvmModule, namedValues);

	codegen.EmitTranslationUnit(&astRoot);

	llvmModule->print(llvm::errs(), nullptr);

	return 0;
}