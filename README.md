Kaleidoscope Compiler (LLVM) (An LLVM front-end for the Kaleidoscope Language)
=====

This is a currently in construction LLVM front-end for the Kaleidoscope language that has been built whilst following through the [tutorial](https://llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html).


How to build?
-----

#### Project Dependencies:

The current project dependecies are:  

- [llvm-project](https://github.com/llvm/llvm-project)
- [ms-gsl](https://github.com/microsoft/GSL)

Management of the dependencies is left to the user but there are straight forwards methods to handle these.

#### Recommended dependency management and consumption:

- Getting LLVM: 
	- clone the repo locally
	- follow the build instructions: [using cmake](https://llvm.org/docs/CMake.html), [building clang at the same time](https://clang.llvm.org/get_started.html)
	- install llvm (also in the above cmake instructions)
- Consuming LLVM:
	- simply provide the path to the directory containing llvmConfig.cmake, using the variable LLVM_CONFIG_DIR, at the command line when calling cmake to generate build files.
		- `cmake .. -DLLVM_CONFIG_DIR="path/to/dir/containing/llvmConfig.cmake"`

- Getting other dependencies:
	- use [VCPKG](https://github.com/microsoft/vcpkg)
	
- Consuming other dependencies:
	- provide the path to the vcpkg.cmake file inside VCPKG, via the varaible CMAKE_TOOLCHAIN_FILE, at the command line when calling cmake to generate build files.
		- `cmake .. -DCMAKE_TOOLCHAIN_FILE="pathToVCPKGRootDir/scrips/buildsystem/vcpkg.cmake"`
	
#### Building:

Using CMake and having set up the dependencies building should be straight forward either at the command line doing an out-of-source build such as:  

- `mkdir build`
- `cd build`
- `cmake .. -DCMAKE_TOOLCHAIN_FILE="path/to/file" -DLLVM_CONFIG_DIR="path/to/directory"`
- `cmake . --build`

or through using your favourite IDE such as Visual Studio's cmake integration.